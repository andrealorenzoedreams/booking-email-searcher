package com.odigeo.email.searcher.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.elasticsearch.ElasticsearchClient;
import com.odigeo.email.searcher.elasticsearch.ElasticsearchClientFactory;
import com.odigeo.email.searcher.util.Constants;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;

@Singleton
public class CustomerEmailSearcher {

    private final ElasticsearchClient elasticsearchClient;

    @Inject
    public CustomerEmailSearcher(ElasticsearchClientFactory elasticsearchClientFactory) {
        this.elasticsearchClient = elasticsearchClientFactory.createClient();
    }

    public SearchResponse searchByBookingId(long bookingId) throws IOException {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.matchQuery(Constants.BOOKING_ID, bookingId));
        SearchRequest searchRequest = new SearchRequest(Constants.SEARCH_INDEX).source(new SearchSourceBuilder().query(queryBuilder));
        return elasticsearchClient.search(searchRequest);
    }

    public SearchResponse searchBetweenPeriod(long init, long end) throws IOException {
        RangeQueryBuilder queryBuilder = QueryBuilders.rangeQuery(Constants.TIMESTAMP);
        queryBuilder.gt(init);
        queryBuilder.lte(end);
        SearchRequest searchRequest = new SearchRequest(Constants.SEARCH_INDEX).source(new SearchSourceBuilder().size(10000).query(queryBuilder));
        return elasticsearchClient.search(searchRequest);
    }
}
