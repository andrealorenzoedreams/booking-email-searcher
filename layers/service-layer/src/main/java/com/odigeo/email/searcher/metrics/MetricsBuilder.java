package com.odigeo.email.searcher.metrics;

import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsPolicy;

public class MetricsBuilder {

    public static Metric buildSentMetric() {
        return new Metric.Builder(MetricsConstants.SEARCHED)
                .policy(MetricsPolicy.Precision.MINUTELY, MetricsPolicy.Retention.TRIMESTER)
                .build();
    }
}
