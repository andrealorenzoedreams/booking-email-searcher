package com.odigeo.email.searcher.mapper;

import com.odigeo.email.searcher.util.Constants;
import com.odigeo.email.searcher.v1.contract.Email;
import com.odigeo.email.searcher.v1.contract.EmailType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SearchHitsMapper {

    public List<Email> mapToEmailList(SearchHits searchHits) {
        return Arrays.stream(searchHits.getHits()).map(hit -> createEmailFromHit(hit)).collect(Collectors.toList());
    }

    private Email createEmailFromHit(SearchHit searchHit) {
        Map<String, Object> mapValues = searchHit.getSourceAsMap();
        String subject = String.valueOf(mapValues.get(Constants.SUBJECT));
        String body = String.valueOf(mapValues.get(Constants.BODY));
        EmailType emailType = EmailType.valueOf(String.valueOf(mapValues.get(Constants.EMAIL_TYPE)));
        Long sentEpochMilli = (Long) mapValues.get(Constants.TIMESTAMP);
        return new Email(subject, body, emailType, sentEpochMilli);
    }
}
