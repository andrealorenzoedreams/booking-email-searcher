package com.odigeo.email.searcher.metrics;

public class MetricsConstants {

    public static final String REGISTRY_BOOKING_EMAIL_SEARCHER_SERVICE = "booking-email-searcher";

    static final String SEARCHED = "searched";

}
