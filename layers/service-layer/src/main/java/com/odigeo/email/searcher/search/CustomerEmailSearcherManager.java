package com.odigeo.email.searcher.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.mapper.SearchHitsMapper;
import com.odigeo.email.searcher.v1.contract.Email;
import org.elasticsearch.action.search.SearchResponse;

import java.io.IOException;
import java.util.List;

@Singleton
public class CustomerEmailSearcherManager {

    private final CustomerEmailSearcher customerEmailSearcher;
    private final SearchHitsMapper searchHitsMapper;

    @Inject
    public CustomerEmailSearcherManager(CustomerEmailSearcher customerEmailSearcher, SearchHitsMapper searchHitsMapper) {
        this.customerEmailSearcher = customerEmailSearcher;
        this.searchHitsMapper = searchHitsMapper;
    }

    public List<Email> searchEmails(long bookingId) throws IOException {
        SearchResponse searchResponse = customerEmailSearcher.searchByBookingId(bookingId);
        return searchHitsMapper.mapToEmailList(searchResponse.getHits());
    }

    public List<Email> searchEmails(long init, long end) throws  IOException {
        SearchResponse searchResponse = customerEmailSearcher.searchBetweenPeriod(init, end);
        return searchHitsMapper.mapToEmailList(searchResponse.getHits());
    }
}
