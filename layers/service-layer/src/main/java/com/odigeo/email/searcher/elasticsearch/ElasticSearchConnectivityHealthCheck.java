package com.odigeo.email.searcher.elasticsearch;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.configuration.ElasticsearchConfiguration;

import javax.net.SocketFactory;
import java.io.IOException;
import java.net.Socket;

@Singleton
public class ElasticSearchConnectivityHealthCheck extends HealthCheck {
    private final ElasticsearchConfiguration elasticsearchConfiguration;

    @Inject
    public ElasticSearchConnectivityHealthCheck(ElasticsearchConfiguration elasticsearchConfiguration) {
        this.elasticsearchConfiguration = elasticsearchConfiguration;
    }

    @Override
    protected Result check() {
        if (isHealthy(elasticsearchConfiguration.getHostName(), elasticsearchConfiguration.getPort())) {
            return Result.healthy("Elasticsearch is up on  " + elasticsearchConfiguration.getHostName() + ":" + elasticsearchConfiguration.getPort());
        } else {
            return Result.unhealthy("Elasticsearch is down on  " + elasticsearchConfiguration.getHostName() + ":" + elasticsearchConfiguration.getPort());
        }
    }

    private boolean isHealthy(String hostName, int port) {
        try {
            Socket pingSocket = SocketFactory.getDefault().createSocket(hostName, port);
            pingSocket.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
