
Feature: Transactional email search by booking id

  Scenario: Search the emails sent to a given booking id

    Given a bookingId 1234567890

    When the search by id is performed

    Then the list of emails sent for that booking Id is retrieved
