Feature: Transactional emails search between period of time

  Scenario: Search the emails sent in a given period of time

    Given a period of time from 01/01/2019 00:00 to 01/01/2019 01:00

    When the search by time period is performed

    Then the list of emails sent between the period of time is retrieved
