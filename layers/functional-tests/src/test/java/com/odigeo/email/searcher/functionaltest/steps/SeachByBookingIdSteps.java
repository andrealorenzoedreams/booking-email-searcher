package com.odigeo.email.searcher.functionaltest.steps;

import com.google.inject.Inject;
import com.odigeo.email.searcher.functionaltest.world.SearcherWorld;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.testng.Assert;

import java.io.IOException;
import java.time.Instant;

@ScenarioScoped
public class SeachByBookingIdSteps {

    private final SearcherWorld searcherWorld;

    @Inject
    public SeachByBookingIdSteps(SearcherWorld searcherWorld) {
        this.searcherWorld = searcherWorld;
    }

    @Given("^a bookingId (.*)")
    public void setAndIndexBookingId(long bookingId) throws IOException {
        searcherWorld.setBookingId(bookingId);
        searcherWorld.indexSync(bookingId, Instant.now().toEpochMilli());
        searcherWorld.indexSync(bookingId, Instant.now().toEpochMilli());

    }

    @Then("^the list of emails sent for that booking Id is retrieved")
    public void checkEmails() {
        Assert.assertEquals(searcherWorld.getEmailList().size(), 2);
    }

}
