package com.odigeo.email.searcher.functionaltest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.commons.test.docker.ContainerInfoBuilderFactory;
import com.odigeo.commons.test.docker.LogFilesTransfer;
import com.odigeo.email.searcher.elasticsearch.ElasticsearchClient;
import com.odigeo.email.searcher.elasticsearch.ElasticsearchInstaller;
import com.odigeo.email.searcher.v1.client.SearchServiceModuleBuilder;
import com.odigeo.technology.docker.ContainerComposer;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.DockerModule;
import com.odigeo.technology.docker.ImageController;
import com.odigeo.technology.docker.ImageException;
import com.odigeo.technology.docker.UrlPingChecker;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;

@CucumberOptions(format = {"pretty", "json:target/cucumber.json", "html:target/cucumber-pretty" }, strict = true,
        tags = {"~@ignore" },
        glue = {"classpath:com/odigeo/email/searcher/functionaltest"},
        features = {"classpath:com/odigeo/email/searcher/functionaltest"}
    )
@Guice(modules = DockerModule.class)
public class FunctionalTest extends AbstractTestNGCucumberTests {

    private static final Logger LOGGER = Logger.getLogger(FunctionalTest.class);
    @Inject
    private ElasticsearchInstaller elasticsearchInstaller;
    @Inject
    private ImageController imageController;
    @Inject
    private ContainerInfoBuilderFactory containerInfoBuilderFactory;
    @Inject
    private ContainerComposer containerComposer;
    @Inject
    private LogFilesTransfer logFilesTransfer;

    private final Artifact bookingEmailSearcher = new Artifact(System.getProperty("module.groupId"), System.getProperty("module.name"), System.getProperty("module.version"));

    static {
        ConfigurationEngine.init(new SearchServiceModuleBuilder().build());
    }

    @BeforeClass
    public void setUp() throws Exception {
        LOGGER.info("Initializing docker containers...");
        LOGGER.info(bookingEmailSearcher.toString());

        try {
            ContainerInfo elasticsearchContainer = new ContainerInfoBuilder("docker.elastic.co/elasticsearch/elasticsearch:6.3.2")
                    .setName("elasticsearch-functional-test")
                    .addPortMapping(ElasticsearchClient.PORT, ElasticsearchClient.PORT)
                    .addEvironmentVariable("discovery.type", "single-node")
                    .setPingChecker(new UrlPingChecker(HttpClientBuilder.create().build(), ElasticsearchClient.HOST, ElasticsearchClient.PORT, ""))
                    .build();

            ContainerInfo searchContainer = containerInfoBuilderFactory
                    .newModule(bookingEmailSearcher)
                    .addDependency(elasticsearchContainer)
                    .build();

            containerComposer.addServiceAndDependencies(searchContainer, 10, 5000).composeUp(true);
        } catch (ContainerException | ImageException e) {
            LOGGER.error("error:", e);
            logFilesTransfer.copyToExternalFolder(bookingEmailSearcher);
            throw e;
        } catch (IllegalArgumentException e) {
            LOGGER.error("error:", e);
            throw e;
        }

        LOGGER.info("Initialized docker containers...");
        elasticsearchInstaller.install();

    }

    @AfterClass
    public void tearDown() throws Exception {
        containerComposer.composeDown();
        imageController.removeFuncionalTestsImage(bookingEmailSearcher.getArtifactId());
    }


}
