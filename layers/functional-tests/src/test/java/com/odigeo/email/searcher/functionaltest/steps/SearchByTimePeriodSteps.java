package com.odigeo.email.searcher.functionaltest.steps;

import com.google.inject.Inject;
import com.odigeo.email.searcher.functionaltest.world.SearcherWorld;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.testng.Assert;

import java.io.IOException;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;


@ScenarioScoped
public class SearchByTimePeriodSteps {
    private final SearcherWorld searcherWorld;

    @Inject
    public SearchByTimePeriodSteps(SearcherWorld searcherWorld) {
        this.searcherWorld = searcherWorld;
    }

    @Given("^a period of time from (.*) to (.*)")
    public void setAndIndexBookingId(String init, String end) throws IOException {
        searcherWorld.setInit(init);
        searcherWorld.setEnd(end);
        searcherWorld.indexSync(1L, searcherWorld.getInit().plus(-4, ChronoUnit.MINUTES).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli());
        searcherWorld.indexSync(2L, searcherWorld.getInit().plus(4, ChronoUnit.MINUTES).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli());
        searcherWorld.indexSync(3L, searcherWorld.getInit().plus(6, ChronoUnit.MINUTES).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli());
        searcherWorld.indexSync(3L, searcherWorld.getEnd().plus(6, ChronoUnit.MINUTES).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli());
    }


    @Then("^the list of emails sent between the period of time is retrieved")
    public void checkEmails() {
        Assert.assertEquals(searcherWorld.getEmailList().size(), 2);
    }
}
