package com.odigeo.email.searcher.functionaltest.steps;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.email.searcher.functionaltest.world.SearcherWorld;
import com.odigeo.email.searcher.v1.contract.Email;
import com.odigeo.email.searcher.v1.contract.SearchService;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.rmi.RemoteException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ScenarioScoped
public class PerformSearchStep {

    private final SearcherWorld searcherWorld;

    @Inject
    public PerformSearchStep(SearcherWorld searcherWorld) {
        this.searcherWorld = searcherWorld;
    }

    @When("^the search by id is performed")
    public void performSearch() throws RemoteException {
        List<Email> emails = new ArrayList<>();
        searcherWorld.getStopwatch().reset().start();

        while (emails.size() < 2 && searcherWorld.getStopwatch().elapsed(TimeUnit.SECONDS) < 5) {
            emails = ConfigurationEngine.getInstance(SearchService.class).search(searcherWorld.getBookingId());
        }

        searcherWorld.getStopwatch().stop();
        searcherWorld.setEmailList(emails);
    }

    @When("^the search by time period is performed")
    public void performSearchByPeriod() throws RemoteException {
        List<Email> emails = new ArrayList<>();
        searcherWorld.getStopwatch().reset().start();
        int count = 0;
        while (emails.size() < 2 && searcherWorld.getStopwatch().elapsed(TimeUnit.SECONDS) < 5) {
            long init = searcherWorld.getInit().atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli();
            long end = searcherWorld.getEnd().atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli();
            emails = ConfigurationEngine.getInstance(SearchService.class).searchBetweenPeriod(init, end);
            count++;
        }
        System.out.println("---> " + count + " elapsed 2 ---> " + searcherWorld.getStopwatch().elapsed(TimeUnit.MILLISECONDS));
        searcherWorld.getStopwatch().stop();
        searcherWorld.setEmailList(emails);
    }
}
