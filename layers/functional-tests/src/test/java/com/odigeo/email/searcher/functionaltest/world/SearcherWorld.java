package com.odigeo.email.searcher.functionaltest.world;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.base.Stopwatch;
import com.odigeo.email.searcher.elasticsearch.ElasticsearchClient;
import com.odigeo.email.searcher.v1.contract.Email;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ScenarioScoped
public class SearcherWorld {

    private static final ElasticsearchClient ELASTICSEARCH_CLIENT = ConfigurationEngine.getInstance(ElasticsearchClient.class);
    private static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm";
    private long bookingId;
    private LocalDateTime init;
    private LocalDateTime end;
    private List<Email> emailList = new ArrayList<>();
    private Stopwatch stopwatch = Stopwatch.createUnstarted();


    public Long getBookingId() {
        return bookingId;
    }

    public SearcherWorld setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public SearcherWorld setEmailList(List<Email> emailList) {
        this.emailList = emailList;
        return this;
    }

    public LocalDateTime getInit() {
        return init;
    }

    public SearcherWorld setInit(String init) {
        this.init = LocalDateTime.parse(init, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN, Locale.getDefault()));
        return this;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public SearcherWorld setEnd(String end) {
        this.end = LocalDateTime.parse(end, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN, Locale.getDefault()));
        return this;
    }

    public void indexSync(long bookingId, long sentEpochMilli) throws IOException {
        ELASTICSEARCH_CLIENT.indexSync(bookingId, sentEpochMilli);
    }

    public Stopwatch getStopwatch() {
        return stopwatch;
    }
}
