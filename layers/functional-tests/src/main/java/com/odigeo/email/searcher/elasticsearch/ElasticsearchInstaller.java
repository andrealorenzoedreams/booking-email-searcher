package com.odigeo.email.searcher.elasticsearch;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.util.Constants;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;

import java.io.IOException;

@Singleton
public class ElasticsearchInstaller {

    private final ElasticsearchClient client;

    @Inject
    public ElasticsearchInstaller(ElasticsearchClient client) {
        this.client = client;
    }

    public void install() throws IOException {
        createTrackingIndex();
    }

    private void createTrackingIndex() throws IOException {
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(Constants.SEARCH_INDEX);
        client.createIndex(createIndexRequest);
    }

}
