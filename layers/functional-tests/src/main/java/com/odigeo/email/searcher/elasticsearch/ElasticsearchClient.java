package com.odigeo.email.searcher.elasticsearch;

import com.odigeo.email.searcher.util.Constants;
import com.odigeo.email.searcher.v1.contract.EmailType;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class ElasticsearchClient {

    public static final int PORT = 9200;
    public static final String HOST = "localhost";
    public static final String SCHEME = "http";
    private final RestHighLevelClient restHighLevelClient;

    public ElasticsearchClient() {
        this.restHighLevelClient = new  RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(
                                HOST,
                                PORT,
                                SCHEME))
        );
    }

    public CreateIndexResponse createIndex(CreateIndexRequest createIndexRequest) throws IOException {
        return restHighLevelClient.indices().create(createIndexRequest);
    }

    public IndexResponse indexSync(long bookingId, long sentMilliSec) throws IOException {
        return restHighLevelClient.index(createIndexDocRequest(bookingId, sentMilliSec));
    }

    private IndexRequest createIndexDocRequest(long bookingId, long sentMilliSec) throws IOException {
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field(Constants.BOOKING_ID, bookingId);
        builder.field(Constants.CUSTOMER_ADDRESS, "address");
        builder.field(Constants.EMAIL_TYPE, EmailType.CONFIRMATION);
        builder.field(Constants.TIMESTAMP, sentMilliSec);
        builder.field(Constants.BOOKING_PRODUCTS, "");
        builder.field(Constants.SUBJECT, "subject");
        builder.field(Constants.BODY, "body");
        builder.endObject();
        return  new IndexRequest(Constants.SEARCH_INDEX, Constants.TRIGGER_TYPE).source(builder);
    }

}
