package com.odigeo.email.searcher.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.searcher.v1.SearcherController;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {
    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        singletons.add(ConfigurationEngine.getInstance(SearcherController.class));
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
