package com.odigeo.email.searcher.v1;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.search.CustomerEmailSearcherManager;
import com.odigeo.email.searcher.v1.contract.Email;
import com.odigeo.email.searcher.v1.contract.SearchService;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;

@Singleton
public class SearcherController implements SearchService {

    private final CustomerEmailSearcherManager customerEmailSearcherManager;

    @Inject
    public SearcherController(CustomerEmailSearcherManager customerEmailSearcherManager) {
        this.customerEmailSearcherManager = customerEmailSearcherManager;
    }

    @Override
    public List<Email> search(long bookingId) throws RemoteException {
        try {
            return customerEmailSearcherManager.searchEmails(bookingId);
        } catch (IOException e) {
            throw new RemoteException("Exception searching emails...", e);
        }
    }

    @Override
    public List<Email> searchBetweenPeriod(long init, long end) throws RemoteException {
        try {
            return customerEmailSearcherManager.searchEmails(init, end);
        } catch (IOException e) {
            throw new RemoteException("Exception searching emails...", e);
        }
    }
}
