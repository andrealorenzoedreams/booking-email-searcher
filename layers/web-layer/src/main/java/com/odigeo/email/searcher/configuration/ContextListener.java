package com.odigeo.email.searcher.configuration;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.edreams.configuration.ConfigurationFilesManager;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.email.searcher.elasticsearch.ElasticSearchConnectivityHealthCheck;
import com.odigeo.email.searcher.metrics.MetricsConstants;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URL;

public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);
    public static final String LOG4J_FILENAME = "/log4j.properties";
    public static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        LOGGER.info("Context initializing...");

        initPropertiesFileBasedConfigurator();

        initLog4J(servletContextEvent);

        initConfigurationEngine(servletContextEvent.getServletContext());

        initMonitoring();

        initHealthChecks(servletContextEvent);

        LOGGER.info("Servlet Context initialized.");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOGGER.info("Context destroyed.");
    }

    private void initPropertiesFileBasedConfigurator() {
        ConfigurationFilesManager.setAppName("booking-email-searcher");
    }

    private void initLog4J(ServletContextEvent servletContextEvent) {
        VirtualFile virtualFile = VFS.getChild(ContextListener.class.getResource(LOG4J_FILENAME).getFile());
        try {
            URL fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
        } catch (IOException e) {
            servletContextEvent.getServletContext().log("Log4j failed to be initialized with configuration file " + LOG4J_FILENAME);
        }
    }

    private void initConfigurationEngine(ServletContext servletContext) {
        HealthCheckRegistry registry = new HealthCheckRegistry();
        ConfigurationEngine.init();
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, registry);
    }

    private void initMonitoring() {
        MetricsManager metricsManager = MetricsManager.getInstance();
        metricsManager.addMetricsReporter(MetricsConstants.REGISTRY_BOOKING_EMAIL_SEARCHER_SERVICE);
        metricsManager.changeReporterStatus(ReporterStatus.STARTED);
    }

    private void initHealthChecks(ServletContextEvent servletContextEvent) {
        final ServletContext servletContext = servletContextEvent.getServletContext();
        HealthCheckRegistry registry = new HealthCheckRegistry();

        ElasticSearchConnectivityHealthCheck elasticSearchConnectivityHealthCheck = ConfigurationEngine.getInstance(ElasticSearchConnectivityHealthCheck.class);
        registry.register("Elasticsearch connection", elasticSearchConnectivityHealthCheck);

        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, registry);
    }
}
