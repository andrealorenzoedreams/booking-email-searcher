package com.odigeo.email.searcher.elasticsearch;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.searcher.configuration.ElasticsearchConfiguration;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

@Singleton
public class ElasticsearchClientFactory {

    private final ElasticsearchConfiguration configuration;

    @Inject
    public ElasticsearchClientFactory(ElasticsearchConfiguration configuration) {
        this.configuration = configuration;
    }

    public ElasticsearchClient createClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(
                                configuration.getHostName(),
                                configuration.getPort(),
                                configuration.getProtocol()))
        );
        return new ElasticsearchClient(client);
    }

}
