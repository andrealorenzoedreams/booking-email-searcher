package com.odigeo.email.searcher.elasticsearch;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

public class ElasticsearchClient {

    private final RestHighLevelClient client;

    public ElasticsearchClient(RestHighLevelClient client) {
        this.client = client;
    }

    public SearchResponse search(SearchRequest searchRequest) throws IOException {
        return client.search(searchRequest);
    }
}
