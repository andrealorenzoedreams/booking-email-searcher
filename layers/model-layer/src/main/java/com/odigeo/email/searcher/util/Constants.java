package com.odigeo.email.searcher.util;

public class Constants {

    public static final String TIMESTAMP = "timestamp";

    public static final String SEARCH_INDEX = "customer";
    public static final String TRIGGER_TYPE = "email";
    public static final String BOOKING_ID = "bookingId";
    public static final String EMAIL_TYPE = "emailType";
    public static final String CUSTOMER_ADDRESS = "customerAddress";
    public static final String BOOKING_PRODUCTS = "bookingProductsInfo";
    public static final String SUBJECT = "subject";
    public static final String BODY = "body";

}
