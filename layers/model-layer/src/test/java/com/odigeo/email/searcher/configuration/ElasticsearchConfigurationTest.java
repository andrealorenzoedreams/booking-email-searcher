package com.odigeo.email.searcher.configuration;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ElasticsearchConfigurationTest {
    private ElasticsearchConfiguration elasticsearchConfiguration;
    private static final String A_STRING = "STRING";
    private static final int A_INTEGER = 0;

    @BeforeMethod
    public void init() {
        ConfigurationEngine.init();
        elasticsearchConfiguration = ConfigurationEngine.getInstance(ElasticsearchConfiguration.class);
    }

    @Test
    public void testGetProtocol() {
        Assert.assertNotNull(elasticsearchConfiguration.getProtocol());
    }

    @Test
    public void testSetProtocol() {
        elasticsearchConfiguration.setProtocol(A_STRING);
        Assert.assertEquals(elasticsearchConfiguration.getProtocol(), A_STRING);
    }

    @Test
    public void testGetHostName() {
        Assert.assertNotNull(elasticsearchConfiguration.getHostName());
    }

    @Test
    public void testSetHostName() {
        elasticsearchConfiguration.setHostName(A_STRING);
        Assert.assertEquals(elasticsearchConfiguration.getHostName(), A_STRING);
    }

    @Test
    public void testGetPort() {
        Assert.assertNotNull(elasticsearchConfiguration.getPort());
    }

    @Test
    public void testSetPort() {
        elasticsearchConfiguration.setPort(A_INTEGER);
        Assert.assertEquals(elasticsearchConfiguration.getPort(), A_INTEGER);
    }
}
